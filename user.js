const User = require("../models/user");

module.exports.checkEmail = (body) => {
    // the mongoDB find() method always returns an array.
    return User.find({email: body.email}).then(result => {
        if (result.length > 0) { // if a duplicate email is found, result.length is 1. otherwise it is 0.
            return true; // true means "yes, email exist".
        } else {
            return false; // false means "no, email dose not exist".
        };
    });
};

// Activity
module.exports.register = (body) => {
    let newUser = new User ({
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email,
        password: body.password,
        mobileNo: body.mobileNo,
    });

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    });
};